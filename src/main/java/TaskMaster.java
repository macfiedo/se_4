import java.util.ArrayList;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

public class TaskMaster {

  private final Queue<String> q;
  private int numberOfProducers;
  private int numberOfConsumers;
  private List<Thread> threads;

  TaskMaster(int numberOfProducers, int numberOfConsumers){
    q = new ConcurrentLinkedQueue<>();
    this.numberOfProducers = numberOfProducers;
    this.numberOfConsumers = numberOfConsumers;
  }

  void start(){
    threads = new ArrayList<>();
    for(int i=0;i<numberOfConsumers;i++){
      threads.add(createConsumerThread());
    }
    for(int i=0;i<numberOfProducers;i++){
      threads.add(createProducerThread());
    }
    for(Thread t : threads){
      t.start();
    }
    for(Thread t : threads){
      try {
        t.join();
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
    }
  }

  private Thread createProducerThread(){
    final TaskProducer producer = new TaskProducer();
    return new Thread(new Runnable() {
      @Override
      public void run() {
        producer.produce(q);
      }
    });
  }

  private Thread createConsumerThread(){
    final TaskConsumer consumer = new TaskConsumer();
    return new Thread(new Runnable() {
      @Override
      public void run() {
        consumer.consume(q);
      }
    });
  }

}
