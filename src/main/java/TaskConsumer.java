
import java.util.LinkedList;
import java.util.Queue;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class TaskConsumer {

  public void consume(Queue<String> queue){
    while(true){
      if(queue.size() > 0) {
        String message = queue.poll();
        System.out.println(processMessage(message));
      }else{
        try {
          Thread.sleep(10);
        } catch (InterruptedException e) {
          e.printStackTrace();
        }
      }
    }
  }

  double processMessage(String message){
    LinkedList<String> elements = splitMessage(message);
    LinkedList<String> resultOfMult = multiplicationAndDividing(elements);
    LinkedList<String> resultOfAdd = addition(resultOfMult);
    return Double.parseDouble(resultOfAdd.peekFirst());
  }

  private LinkedList<String> multiplicationAndDividing(LinkedList<String> elements){
    LinkedList<String> resultList = new LinkedList<>();
    Double firstArgument;
    Double secondArgument = null;
    String operator = null;
    firstArgument = elements.isEmpty() ? null : Double.parseDouble(elements.pollFirst());
    if(!elements.isEmpty()) {
      while (!elements.isEmpty()) {
        String element = elements.pollFirst();
        if (element.matches("-?[0-9]+")) {
          secondArgument = Double.parseDouble(element);
        } else if (element.matches("[-*+/]")) {
          operator = element;
        }
        if (secondArgument != null) {
          if (operator == null) {
            operator = "+";
          }
          if (operator.matches("[*/]")) {
            firstArgument = performCalculation(firstArgument, secondArgument, operator);
            if (elements.isEmpty()) {
              resultList.add(firstArgument.toString());
            }
          } else {
            resultList.add(firstArgument.toString());
            resultList.add(operator);
            if (elements.isEmpty()) {
              resultList.add(secondArgument.toString());
            } else {
              firstArgument = secondArgument;
            }
          }
          secondArgument = null;
          operator = null;
        }
      }
    }else{
      resultList.add(firstArgument.toString());
    }
    return resultList;
  }

  private LinkedList<String> addition(LinkedList<String> elements) {
    //Iterator<String> iter = elements.descendingIterator();
    LinkedList<String> resultList = new LinkedList<>();
    Double firstArgument;
    Double secondArgument = null;
    String operator = null;
    if(elements.size() == 1){
      return elements;
    }else {
      firstArgument = Double.parseDouble(elements.pop());
      while (!elements.isEmpty()) {
        String element = elements.pollFirst();
        if (element.matches("-?[0-9]+\\.[0-9]+")) {
          if (secondArgument == null) {
            secondArgument = Double.parseDouble(element);
          } else {
            operator = "+";
          }
        } else if (element.matches("[-*+/]")) {
          operator = element;
        }
        if (operator != null && secondArgument != null) {
          if (operator.matches("[-+]")) {
            firstArgument = performCalculation(firstArgument, secondArgument, operator);
          }
          secondArgument = null;
          operator = null;
        }
      }
    }
    resultList.add(firstArgument.toString());
    return resultList;
  }

  LinkedList<String> splitMessage(String s){
    LinkedList<String> result = new LinkedList<>();
    if(s != null) {
      s = s.replace(" ", "");
      if (s.matches("[-0-9+/*]*")) {
        Matcher m = Pattern.compile("(-?[0-9]+)|[-+/*]?").matcher(s);
        while (m.find()) {
          String entry = m.group();
          if (!"".equals(entry)) {
            result.add(m.group());
          }
        }
      }
    }
    return result;
  }

  Double performCalculation(Double firstArgument, Double secondArgument, String operator){
    switch (operator){
      case "+":
        return firstArgument + secondArgument;
      case "/":
        if(secondArgument == 0){
          throw new IllegalArgumentException("Divider is zero!");
        }else{
          return firstArgument / secondArgument;
        }
      case "*":
        return firstArgument * secondArgument;
      case "-":
        return firstArgument - secondArgument;
      default:
        throw new IllegalArgumentException("Operand not recognized!");
    }
  }

}
