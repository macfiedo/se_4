import java.util.Queue;
import java.util.concurrent.ThreadLocalRandom;

public class TaskProducer{

  private static final char[] OPERATORS = new char[]{'+', '/', '*','-'};
  private static final int MAX_OPERATIONS = 10;
  private static final int MAX_QUEUE_SIZE = 100;


  void produce(Queue<String> queue){
    while(true){
      if(queue.size() < MAX_QUEUE_SIZE){
        queue.add(createMessage());
      }else{
        try {
          Thread.sleep(10);
        } catch (InterruptedException e) {
          e.printStackTrace();
        }
      }
    }
  }

  private String createMessage(){
    int numberOfOperationsInMessage = ThreadLocalRandom.current().nextInt(MAX_OPERATIONS);
    StringBuilder builder = new StringBuilder();
    builder.append(ThreadLocalRandom.current().nextInt());
    while(numberOfOperationsInMessage-- > 0){
      int characterIndex = ThreadLocalRandom.current().nextInt(OPERATORS.length);
      builder.append(OPERATORS[characterIndex]);
      builder.append(ThreadLocalRandom.current().nextInt());
    }
    return builder.toString();
  }
}
