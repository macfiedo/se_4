import org.junit.Test;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.*;

public class TaskConsumerTest {

  private TaskConsumer consumer;

  private static final Map<String, Double> dataSet;
  static{
    Map<String, Double> tmpMap = new HashMap<>();
    tmpMap.put("2+3", 5.0d);
    tmpMap.put("2*3", 6.0d);
    tmpMap.put("2/3", 0.66667);
    tmpMap.put("2-3", -1.0);
    tmpMap.put("2--3", 5.0);
    tmpMap.put("10+11*12", 142.0);
    tmpMap.put("10*11-12", 98.0);
    tmpMap.put("10/11*12", 10.90909);
    tmpMap.put("10+11-12", 9.0);
    tmpMap.put("10+11*12-14", 128.0);
    tmpMap.put("10*11-12/14", 109.1428571);
    tmpMap.put("10/11*12+14", 24.90909);
    tmpMap.put("10+11-12*14", -147.0);
    tmpMap.put("-2103143522", -2103143522.0);
    dataSet = Collections.unmodifiableMap(tmpMap);
  }

  @org.junit.Before
  public void setUp() {
    consumer = new TaskConsumer();
  }

  @org.junit.Test
  public void processMessageTest() {
    for(Map.Entry entry : dataSet.entrySet()) {
      testMessageProcessing((String)entry.getKey(), (Double)entry.getValue());
    }
  }

  private void testMessageProcessing(String message, Double expectedValue){
    double result = consumer.processMessage(message);
    assertEquals("Expected " + expectedValue + ", but receive " + result + "!", expectedValue, result, 0.001);
  }

  @Test
  public void splitMessageTest(){
    String message = "2+3";
    List<String> result = consumer.splitMessage(message);
    assertEquals(3, result.size());
    assertEquals("2", result.get(0));
    assertEquals("+", result.get(1) );
    assertEquals("3", result.get(2));
  }

  @Test
  public void splitMessageSubstractionTest(){
    String message = "2-3";
    List<String> result = consumer.splitMessage(message);
    assertEquals(2, result.size());
    assertEquals("2", result.get(0));
    assertEquals("-3", result.get(1));
  }

  @Test
  public void splitMessageWithNullString(){
    String message = null;
    List<String> result = consumer.splitMessage(message);
    assertEquals(0, result.size());
  }

  @Test
  public void splitMessageWithEmptyString(){
    String message = "";
    List<String> result = consumer.splitMessage(message);
    assertEquals(0, result.size());
  }

  @Test
  public void splitMessageWithForbiddenCharacters(){
    String message = "12abcd";
    List<String> result = consumer.splitMessage(message);
    assertEquals( 0, result.size());
  }

  @Test
  public void splitMessageWithBiggerNumbers(){
    String message = "12+14";
    List<String> result = consumer.splitMessage(message);
    assertEquals(3, result.size());
    assertEquals("12",result.get(0));
    assertEquals("+", result.get(1));
    assertEquals("14", result.get(2));
  }

  @Test
  public void splitMessageWithNegativeNumbers(){
    String message = "-12+-14";
    List<String> result = consumer.splitMessage(message);
    assertEquals(3, result.size());
    assertEquals("-12",result.get(0));
    assertEquals("+", result.get(1));
    assertEquals("-14", result.get(2));
  }

  @Test
  public void splitMessageWithSubstractionOfNegativenumber(){
    String message = "-12--14";
    List<String> result = consumer.splitMessage(message);
    assertEquals(3, result.size());
    assertEquals("-12",result.get(0));
    assertEquals("-", result.get(1));
    assertEquals("-14", result.get(2));
  }

  @Test
  public void splitMessageWithSubstractionOfNegativenumber2(){
    String message = "5-12";
    List<String> result = consumer.splitMessage(message);
    assertEquals(2, result.size());
    assertEquals("5",result.get(0));
    assertEquals("-12",result.get(1));
  }
}